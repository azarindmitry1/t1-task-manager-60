package ru.t1.azarin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDto task;

    public AbstractTaskResponse(@Nullable TaskDto task) {
        this.task = task;
    }

}
