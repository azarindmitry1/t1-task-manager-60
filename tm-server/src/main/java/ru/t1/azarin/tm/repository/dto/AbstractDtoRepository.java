package ru.t1.azarin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.dto.IDtoRepository;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@NoArgsConstructor
public abstract class AbstractDtoRepository<M extends AbstractDtoModel> implements IDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
