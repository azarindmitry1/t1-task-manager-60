package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.azarin.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedDtoModel;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedDtoModel, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

}
