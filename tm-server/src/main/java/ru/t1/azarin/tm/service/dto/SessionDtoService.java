package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.azarin.tm.api.service.dto.ISessionDtoService;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public final class SessionDtoService extends AbstractDtoService<SessionDto, ISessionDtoRepository> implements ISessionDtoService {

    @NotNull
    @Autowired
    public ISessionDtoRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final SessionDto model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<SessionDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public SessionDto findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final SessionDto model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDto session = findOneById(userId, id);
        if (session == null) throw new EntityNotFoundException();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable final SessionDto model) {
        if (model == null) throw new EntityNotFoundException();
        repository.update(model);
    }

}
