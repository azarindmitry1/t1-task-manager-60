package ru.t1.azarin.tm.service.model;

import lombok.NoArgsConstructor;
import ru.t1.azarin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.azarin.tm.api.service.model.IUserOwnedService;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

}
